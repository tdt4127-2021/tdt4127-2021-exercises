# Part 1
def ask_question(str):
        return input(str)

# Part 2
def spam_with_questions(str):
        answer = ""
        while answer.lower() != 'stop':
                answer = ask_question(str)

# Part 3
def energy_efficient_spamming(str1, str2):
        if len(str2) > len(str1):
                return
        answer = input(str1)
        if answer.lower() != "stop":
                spam_with_questions(str2)
