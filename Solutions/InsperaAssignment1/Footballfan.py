result = input("What was the result? ").lower()

if result == 'h':
    print("Yay, we won!")
elif result == 'b':
    print("Oufh, too bad!.")
elif result == 'u':
    print("Snufs, but it could be worse.")
else:
    print('It was that bad? You do not even want to talk about it...')