# Del 1
def correct_word(word):
    return word.isalpha()

print(correct_word("Ent3r"))
print(correct_word("TDT4109"))
print(correct_word("cyvernetics"))


# Del 2
def count_letters(word):
    if correct_word(word):
        return len(word)
    return -1

print(count_letters("Ouagadougou"))
print(count_letters("ITGK<3")) 
