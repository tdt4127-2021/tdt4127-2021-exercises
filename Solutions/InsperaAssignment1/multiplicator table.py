def print_table(n):
    for i in range(1,n+1):
        print() # Creates a new line
        for j in range(1,n+1):
            print(i*j, end=" ")

print_table(4)